//
//  RTWHomeViewController.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import SwiftUI

struct RTWHomeViewController: View {
    var body: some View {
        Text("Welcome to Home")
    }
}

struct RTWHomeViewController_Previews: PreviewProvider {
    static var previews: some View {
        RTWHomeViewController()
    }
}
