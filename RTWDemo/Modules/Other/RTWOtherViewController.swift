//
//  RTWOtherViewController.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import SwiftUI

struct RTWOtherViewController: View {
    
    @EnvironmentObject var dataStore: RTWDataStore
    
    var body: some View {
        VStack {
            Text("Other")
            Button(action: {
                    // For use with property wrapper
                    UserDefaults.standard.set(false, forKey: "Loggedin")
                    UserDefaults.standard.synchronize()
                    self.dataStore.loggedIn = false
            }) {
                buttonWithBackground(btnText: "Logout")
            }.padding(.vertical, 40)
            
        }
    }
    
}

struct RTWOtherViewController_Previews: PreviewProvider {
    static var previews: some View {
        RTWOtherViewController()
    }
}
