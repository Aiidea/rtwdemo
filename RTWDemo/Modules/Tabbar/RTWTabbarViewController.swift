//
//  RTWTabbarViewController.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import SwiftUI

struct RTWTabbarViewController: View {
    @State private var selectedView = 0
    
    init() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        TabView(selection: $selectedView) {
            NavigationView {
                RTWHomeViewController()
                    .navigationBarTitle("Home", displayMode: .inline)
            }
            .tabItem {
                Image.init("icon_tab_home", tintColor: .clear)
                Text("HOME")
            }.tag(0)
            NavigationView {
                RTWOtherViewController()
                .navigationBarTitle("Other", displayMode: .inline)
            }
            .tabItem {
                Image.init("icon_tab_other", tintColor: .clear)
                Text("OTHER")
            }.tag(1)
        }
        .accentColor(lightblueColor)
    }
}

struct RTWTabbarViewController_Previews: PreviewProvider {
    static var previews: some View {
        RTWTabbarViewController()
    }
}

extension Image {
    init(_ named: String, tintColor: UIColor) {
        let uiImage = UIImage(named: named) ?? UIImage()
        let tintedImage = uiImage.withTintColor(tintColor,
                                                renderingMode: .alwaysTemplate)
        self = Image(uiImage: tintedImage)
    }
}
