//
//  RTWVerifyEmailView.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import SwiftUI
import Apollo

let apollo = ApolloClient(url: URL(string: "http://graphql.dev.rtw.team/query")!)

struct RTWVerifyEmailView: View {
    
    @EnvironmentObject var dataStore: RTWDataStore
    
    @State var email: String = "bananog640@xmail2.net"
    @State var password: String = "helloxiaohou123#"
    @State var token: String = ""
    @State var alertMsg = ""
    @State var showAlert = false
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    init() {
        // TODO: can't subcribe TempMail
        // self.getEmailAddress()
    }
    
    
    var alert: Alert {
        Alert(title: Text(""), message: Text(alertMsg), dismissButton: .default(Text("OK")))
    }
    
    var body: some View {
        
        VStack {
            VStack {
                Spacer(minLength: RTWScale * 15)
                LogoImage()
                Spacer(minLength: RTWScale * 15)
                VStack {
                    HStack {
                        Image("icon_email")
                            .padding(.leading, RTWScale * 20)
                        TextField("Email", text: $email)
                            .frame(height: RTWScale * 40, alignment: .center)
                            .padding(.leading, RTWScale * 10)
                            .padding(.trailing, RTWScale * 10)
                            .font(.system(size: RTWScale * 15, weight: .regular, design: .default))
                            .imageScale(.small)
                            .keyboardType(.emailAddress)
                            .autocapitalization(UITextAutocapitalizationType.none)
                    }
                    seperator()
                }
                Spacer(minLength: RTWScale * 15)
                VStack {
                    HStack {
                        Image("icon_password")
                            .padding(.leading, RTWScale * 20)
                        SecureField("Password", text: $password)
                            .frame(height: RTWScale * 40, alignment: .center)
                            .padding(.leading, RTWScale * 10)
                            .padding(.trailing, RTWScale * 10)
                            .font(.system(size: RTWScale * 15, weight: .regular, design: .default))
                            .imageScale(.small)
                    }
                    seperator()
                }
                Spacer(minLength: RTWScale * 50)
                VStack {
                    Button(action: {
                        if self.isValidInputs() {
                            self.userRegist()
                        }
                    }) {
                        buttonWithBackground(btnText: "Verify")
                    }
                    Spacer(minLength: RTWScale * 35)
                    Text("When I use TEMPMAIL, I find that credit card registration is required, but I do not have a credit card, so I cannot complete the API call provided by TEMPMAIL, but I have completed the logic in the project, please check it, thank you!")
                    .font(.system(size: RTWScale * 15, weight: .light, design: .default))
                    .padding(.horizontal, RTWScale * 20)
                    .foregroundColor(lightblueColor)
                    Spacer()
                }
            }
            .alert(isPresented: $showAlert, content: { self.alert })
        }
    }
    
    fileprivate func isValidInputs() -> Bool {
        
        self.alertMsg = ""
        if self.email == "" {
            self.alertMsg = "Email can't be blank."
        } else if !self.email.isValidEmail {
            self.alertMsg = "Email is not valid."
        } else if self.password == "" {
            self.alertMsg = "Password can't be blank."
        } else if !(self.password.isValidPassword) {
            self.alertMsg = "Please enter valid password"
        }
        if self.alertMsg.isBlank {
            return true
        }
        self.showAlert.toggle()
        return false
    }
    
    fileprivate func userRegist() {
        apollo.perform(mutation: UserRegistrationMutation(email: self.email, password: self.password)){ result in
            print(result)
            guard let data = try? result.get().data else { return }
            switch data.userRegistration.status {
            case .success :
                self.getToken()
                break
            case .fail :
                self.alertMsg = data.userRegistration.message ?? "error"
                self.showAlert.toggle()
                break
            case .__unknown(_): break
            }
        }
    }
    
    fileprivate func verifyEmail() {
        apollo.perform(mutation: UserVerifyEmailMutation(email: self.email, token: self.token)){ result in
            guard let data = try? result.get().data else { return }
            switch data.userVerifyEmail.status {
            case .success :
                UserDefaults.standard.set(true, forKey: "Loggedin")
                UserDefaults.standard.synchronize()
                self.dataStore.loggedIn = true
                print(data)
                break
            case .fail :
                self.alertMsg = data.userVerifyEmail.message ?? "error"
                self.showAlert.toggle()
                break
            case .__unknown(_): break
            }
        }
    }
    
    fileprivate func getEmailAddress() {
        RTWEmailManager.sharedInstance.getEmailAddress(success: { (email) in
            self.email = email;
        }) { (error) in
            
        }
    }
    
    fileprivate func getToken() {
        RTWEmailManager.sharedInstance.getToken(success: { (token) in
            self.token = token
            self.verifyEmail()
        }) { (error) in
            
        }
    }
}

struct RTWVerifyEmailView_Previews: PreviewProvider {
    static var previews: some View {
        RTWVerifyEmailView()
    }
}
