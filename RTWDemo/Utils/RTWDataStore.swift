//
//  RTWDataStore.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import SwiftUI
import Combine

final class RTWDataStore: ObservableObject {
    
    let didChange = PassthroughSubject<RTWDataStore, Never>()
    
    @Published var login: Bool = false
    
    @RTWUserDefault(key: "loggedIn", defaultValue: false)
    var loggedIn: Bool {
        didSet {
            didChange.send(self)
            self.login = self.loggedIn
        }
    }
}
