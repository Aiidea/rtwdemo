//
//  RTWEmailManager.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/26.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import Foundation

class RTWEmailManager {
    
    static let sharedInstance: RTWEmailManager = {
        let instance = RTWEmailManager()
        return instance
    }()
    
    func getEmailAddress(success: @escaping(_ email: String) -> (), failture: @escaping(_ error : Error) -> ()) {
        let headers = [
            "x-rapidapi-host": "privatix-temp-mail-v1.p.rapidapi.com",
            "x-rapidapi-key": "ee633f0f5bmsh520236f1f0c8feap1f2201jsn8c2d669cd1b8"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://privatix-temp-mail-v1.p.rapidapi.com/request/mail/id/202cb962ac59075b964b07152d234b70/")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                failture(error!)
            } else {
                // do something parse response
                success("")
            }
        })

        dataTask.resume()
    }


    func getToken(success: @escaping(_ token: String) -> (), failture: @escaping(_ error : Error) -> ()) {
        let headers = [
            "x-rapidapi-host": "privatix-temp-mail-v1.p.rapidapi.com",
            "x-rapidapi-key": "ee633f0f5bmsh520236f1f0c8feap1f2201jsn8c2d669cd1b8"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://privatix-temp-mail-v1.p.rapidapi.com/request/one_mail/id/202cb962ac59075b964b07152d234b70/")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                failture(error!)
            } else {
                // do something parse response
                success("")
            }
        })

        dataTask.resume()
    }
    
}


