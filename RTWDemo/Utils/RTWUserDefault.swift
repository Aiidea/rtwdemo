//
//  RTWUserDefault.swift
//  RTWDemo
//
//  Created by 侯荡荡 on 2019/11/24.
//  Copyright © 2019 houdangdang. All rights reserved.
//

import Foundation

@propertyWrapper

struct RTWUserDefault<T> {
    
    let key: String
    let defaultValue: T
    
    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        } set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}
